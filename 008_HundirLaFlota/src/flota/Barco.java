package flota;

public class Barco {

    //Atributos
    private String nombreBarco;
    private String color;
    private int largo;
    private boolean hundido = false;

    //Metodo constructor
    public Barco(String nombreBarco, String color, int largo, boolean hundido) {

        this.nombreBarco = nombreBarco;
        this.color = color;
        this.largo = largo;
        this.hundido = hundido;

    }

    //Settes and Getters
    public void setNombreBarco(String nombreBarco) {
        this.nombreBarco = nombreBarco;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public void setHundido(boolean hundido) {
        this.hundido = hundido;
    }

    public String getNombreBarco() {
        return nombreBarco;
    }

    public String getColor() {
        return color;
    }

    public int getLargo() {
        return largo;
    }

    public boolean isHundido() {
        return hundido;
    }

    public void tocado() {
        largo--;
        if (largo == 0) hundido = true;
    }
}
