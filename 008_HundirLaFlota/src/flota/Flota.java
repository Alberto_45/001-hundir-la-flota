package flota;

import java.util.ArrayList;

public class Flota {

    private int portaviones = 1;
    private int acorazado = 2;
    private int buque = 3;
    private int submarino = 4;

    private ArrayList<Barco> flotaBarcos = new ArrayList<>(10);

    public Flota() {
        flotaBarcos.add(new Barco("portaviones", "Gris", 4, false));
        flotaBarcos.add(new Barco("acorazado", "Azul marino", 3, false));
        flotaBarcos.add(new Barco("acorazado", "Azul marino", 3, false));
        flotaBarcos.add(new Barco("buque", "rojo", 2, false));
        flotaBarcos.add(new Barco("buque", "rojo", 2, false));
        flotaBarcos.add(new Barco("buque", "rojo", 2, false));
        flotaBarcos.add(new Barco("submarino", "verde", 1, false));
        flotaBarcos.add(new Barco("submarino", "verde", 1, false));
        flotaBarcos.add(new Barco("submarino", "verde", 1, false));
        flotaBarcos.add(new Barco("submarino", "verde", 1, false));

    }

    public ArrayList<Barco> getFlotaBarcos() {
        return flotaBarcos;
    }

    public void setFlotaBarcos(ArrayList<Barco> flotaBarcos) {
        this.flotaBarcos = flotaBarcos;
    }

    public Barco getBarco(int index) {
        return flotaBarcos.get(index);
    }

    public void contadorBarcos() {

        for (Barco flotaBarco : flotaBarcos) {
            if (flotaBarco.isHundido() && flotaBarco.getNombreBarco().equals("portaviones"))
                portaviones--;
            else if (flotaBarco.isHundido() && flotaBarco.getNombreBarco().equals("acorazado"))
                acorazado--;
            else if (flotaBarco.isHundido() && flotaBarco.getNombreBarco().equals("buque"))
                buque--;
            else if (flotaBarco.isHundido() && flotaBarco.getNombreBarco().equals("submarino"))
                submarino--;
        }
    }


    public void eliminarBarco(int barco) {
        flotaBarcos.remove(barco);
    }
}