package flota;

import java.util.Arrays;

public class Tablero {

    //Atributos
    private int[][] tableroInferior = new int[10][10];

    private String[][] tableroSuperior = new String[10][10];

    private boolean[][] verificarTocado = new boolean[10][10];

    //Metodo constructor
    public Tablero() {
        prepararTablero();
    }

    //Setters and Getters
    public int[][] getTableroInferior() {
        return tableroInferior;
    }

    public void setTableroInferior(int[][] tableroInferior) {
        this.tableroInferior = tableroInferior;
    }

    public String[][] getTableroSuperior() {
        return tableroSuperior;
    }

    public void setTableroSuperior(String[][] tableroSuperior) {
        this.tableroSuperior = tableroSuperior;
    }

    /**
     * Muestra por consola un tablero del juego Hundir la Flota, en una matriz de tipo numeros enteros.
     * En este caso la matriz la llena de [0] que representa el "agua" en el juego, la cual nos servira
     * de guia.
     */
    public void mostrarTableroInf() {
        for (int i = 0; i < tableroInferior.length; i++) {
            for (int j = 0; j < tableroInferior[i].length; j++) {
                System.out.print("[" + tableroInferior[i][j] + "]");
            }
            System.out.println();
        }
    }

    /**
     * Muestra por consola un tablero del juego Hundir la Flota, en una matriz de tipo numeros enteros.
     * En ella se iran colocanso los "barcos". De tal forma que si en la posicion de la matriz es 'null'
     * colocora un [ ] en esa posicion. En el caso contrario colocara lo que contenga la matriz esa posicion.
     * ejemplo: [1]
     */
    public void mostrarTableroSup() {

        String letra = "A";
        int convertir;

        System.out.println("   0  1  2  3  4  5  6  7  8  9");
        System.out.print("A ");

        for (int i = 0; i < tableroSuperior.length; i++) {
            for (int j = 0; j < tableroSuperior[i].length; j++) {

                if (tableroSuperior[i][j] == null) System.out.print("[ ]");

                else System.out.print("[" + tableroSuperior[i][j] + "]");
            }
            System.out.println();
            if (i < 9) {
                convertir = letra.charAt(0) + (i + 1);
                System.out.print((char) convertir + " ");
            }

        }
    }

    public void prepararTablero() {
        for (int i = 0; i < tableroInferior.length; i++) {
            Arrays.fill(tableroInferior[i], -2);
        }

    }

    public String mostrarTableroGenerico(int[][] tabInf) {
        String tablero = "\n";
        for (int i = 0; i < tabInf.length; i++) {
            for (int j = 0; j < tabInf[i].length; j++) {
                if (tabInf[i][j] < 0) {
                    tablero = tablero.concat("[" + tabInf[i][j] + "]");
                } else {
                    tablero = tablero.concat("[ " + tabInf[i][j] + "]");
                }
            }
            tablero = tablero.concat("\n");
        }
        return tablero;
    }

    public void marcarTableroSuperior(int fila, int columna, String marca) {
        tableroSuperior[fila][columna] = marca;
    }

    public void marcarPosicion(int fila, int columna) {
        verificarTocado[fila][columna] = true;
    }

    public boolean comprobarPosicion(int fila, int columna) {
        return verificarTocado[fila][columna];
    }

}
