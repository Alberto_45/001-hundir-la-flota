package flota;

public class Jugador {

    private int nivelDifilcultad;
    private String nombre;
    private int numMovimentos;


    public Jugador(int nivelDifilcultad,String nombre){

        this.nombre = nombre;
        this.nivelDifilcultad = nivelDifilcultad;
        if(nivelDifilcultad == 0) numMovimentos = 60;
        else if(nivelDifilcultad == 1) numMovimentos = 50;
        else if(nivelDifilcultad == 2) numMovimentos = 35;
        else if(nivelDifilcultad == 9) numMovimentos = 3;

    }

    public int getNivelDifilcultad() {
        return nivelDifilcultad;
    }

    public void setNivelDifilcultad(int nivelDifilcultad) {
        this.nivelDifilcultad = nivelDifilcultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumMovimentos() {
        return numMovimentos;
    }

    public void setNumMovimentos(int numMovimentos) {
        this.numMovimentos = numMovimentos;
    }

    public void restarMovimiento(){
        numMovimentos--;
    }


}
