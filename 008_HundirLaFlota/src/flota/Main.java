package flota;

import org.apache.log4j.Logger;

public class Main {

    final static Logger logger = Logger.getLogger(Partida.class);

    public static void main(String[] args) {


        System.out.println("INICIANDO HUNDIR LA FLOTA");

        Jugador player = new Jugador(2,"Alberto");

        System.out.println("Jugador: "+player.getNombre()+" ### NIVEL DE DIFICULTAD: "+player.getNivelDifilcultad());

        Partida partida = new Partida(player);
        partida.prepararPartida();

        partida.iniciarPartida();

        System.out.println("SALIENDO DE HUNDIR LA FLOTA");
    }
}
