package flota;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Partida {

    Scanner scan = new Scanner(System.in);

    final static Logger logger = Logger.getLogger(String.valueOf(Partida.class));

    Tablero tablero;
    Flota flota;
    Jugador jugador;
    boolean barcoColocado = false;
    String conversor = "ABCDEFGHIJ";

    public Partida(Jugador jugador) {

        this.tablero = new Tablero();
        this.flota = new Flota();
        this.jugador = jugador;
    }

    public Tablero getTablero() {
        return tablero;
    }

    public void setTablero(Tablero tablero) {
        this.tablero = tablero;
    }

    public Flota getFlota() {
        return flota;
    }

    public void setFlota(Flota flota) {
        this.flota = flota;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public void prepararPartida() {
        logger.debug("########### PREPARANDO PARTIDA ###########");
        Random rnd = new Random();
        int fila, columna;
        int direccion;
        int[][] tabInf = tablero.getTableroInferior();
        ArrayList<Barco> vectorBarcos = flota.getFlotaBarcos();

        for (int i = 0; i < vectorBarcos.size(); i++) {

            while (!barcoColocado) {

                fila = rnd.nextInt(10);
                columna = rnd.nextInt(10);

                if (tabInf[fila][columna] == -2) {

                    direccion = rnd.nextInt(4);

                    switch (direccion) {

                        case 0://Norte
                            logger.debug("Voy a colocar el " + vectorBarcos.get(i).getNombreBarco() + " hacia el Norte");
                            if (comprobarNorte(vectorBarcos.get(i).getLargo(), fila, columna, vectorBarcos.get(i).getNombreBarco())) {
                                logger.debug("#Procedo a colocar el barco#");
                                colocarBarcoNorte(i, vectorBarcos.get(i).getLargo(), fila, columna, tabInf);
                                logger.info(tablero.mostrarTableroGenerico(tabInf));
                                barcoColocado = true;
                            } else

                                logger.warn("No he podido colocar el " + vectorBarcos.get(i).getNombreBarco() + ", vuelvo a empezar");
                            break;

                        case 1://Sur
                            logger.debug("Voy a colocar el " + vectorBarcos.get(i).getNombreBarco() + " hacia el Sur");
                            if (comprobarSur(vectorBarcos.get(i).getLargo(), fila, columna, vectorBarcos.get(i).getNombreBarco())) {
                                logger.debug("#Procedo a colocar el barco#");
                                colocarBarcoSur(i, vectorBarcos.get(i).getLargo(), fila, columna, tabInf);
                                logger.info(tablero.mostrarTableroGenerico(tabInf));
                                barcoColocado = true;
                            } else
                                logger.warn("No he podido colocar el " + vectorBarcos.get(i).getNombreBarco() + ", vuelvo a empezar");
                            break;

                        case 2://Este
                            logger.debug("Voy a colocar el " + vectorBarcos.get(i).getNombreBarco() + " hacia el Este");
                            if (comprobarEste(vectorBarcos.get(i).getLargo(), fila, columna, vectorBarcos.get(i).getNombreBarco())) {
                                logger.debug("#Procedo a colocar el barco#");
                                colocarBarcoEste(i, vectorBarcos.get(i).getLargo(), fila, columna, tabInf);
                                logger.info(tablero.mostrarTableroGenerico(tabInf));
                                barcoColocado = true;
                            } else
                                logger.warn("No he podido colocar el " + vectorBarcos.get(i).getNombreBarco() + ", vuelvo a empezar");
                            break;

                        case 3://Oeste
                            logger.debug("Voy a colocar el " + vectorBarcos.get(i).getNombreBarco() + " hacia el Oeste");
                            if (comprobarOeste(vectorBarcos.get(i).getLargo(), fila, columna, vectorBarcos.get(i).getNombreBarco())) {
                                logger.debug("#Procedo a colocar el barco#");
                                colocarBarcoOeste(i, vectorBarcos.get(i).getLargo(), fila, columna, tabInf);
                                logger.info(tablero.mostrarTableroGenerico(tabInf));
                                barcoColocado = true;
                            } else
                                logger.warn("No he podido colocar el " + vectorBarcos.get(i).getNombreBarco() + ", vuelvo a empezar");
                            break;
                    }
                    tablero.setTableroInferior(tabInf);
                }
            }
            barcoColocado = false;
        }
        System.out.println("MAPA GENERADO");
    }


    private void colocarBarcoOeste(int num, int largo, int fila, int columna, int[][] tablero) {
        //Tapa izquierda
        if (columna - largo > -1) {
            if (fila - 1 > -1) tablero[fila - 1][columna - largo] = -1;
            tablero[fila][columna - largo] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna - largo] = -1;
        }

        //Tapa arriba, abajo y colocar barco
        for (int i = 0; i < largo; i++) {
            logger.info("Voy a colocar el barco en el punto [" + fila + "][" + (columna - i) + "]");
            tablero[fila][columna - i] = num;
            if (fila - 1 > -1) tablero[fila - 1][columna - i] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna - i] = -1;
        }
        //Tapa derecha
        if (columna + 1 < 10) {
            if (fila - 1 > -1) tablero[fila - 1][columna + 1] = -1;
            tablero[fila][columna + 1] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna + 1] = -1;
        }
    }

    private boolean comprobarOeste(int largo, int fila, int columna, String nombreBarco) {
        //crear boolean igual a true
        boolean oeste = true;
        //bucle de comprobacion
        for (int i = 0; i < largo; i++) {

            logger.debug("Voy a comprobar el punto [" + fila + "][" + (columna - i) + "] para el " + nombreBarco);
            if (columna - i < 0 || tablero.getTableroInferior()[fila][columna - i] != -2) {
                oeste = false;
                break;
            }
        }
        //return de boolean
        return oeste;
    }

    private void colocarBarcoEste(int num, int largo, int fila, int columna, int[][] tablero) {
        //Tapa izquierda
        if (columna - 1 > -1) {
            if (fila - 1 > -1) tablero[fila - 1][columna - 1] = -1;
            tablero[fila][columna - 1] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna - 1] = -1;
        }
        //Tapa arriba, abajo y colocar barco
        for (int i = 0; i < largo; i++) {
            logger.info("Voy a colocar el barco en el punto [" + fila + "][" + (columna + i) + "]");
            tablero[fila][columna + i] = num;
            if (fila - 1 > -1) tablero[fila - 1][columna + i] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna + i] = -1;
        }
        //Tapa derecha
        if(columna+largo < 10) {
            if (fila - 1 > -1 ) tablero[fila - 1][columna + largo] = -1;
            tablero[fila][columna + largo] = -1;
            if (fila + 1 < 10) tablero[fila + 1][columna + largo] = -1;
        }
    }

    private boolean comprobarEste(int largo, int fila, int columna, String nombreBarco) {
        //crear boolean igual a true
        boolean este = true;
        //bucle de comprobacion
        for (int i = 0; i < largo; i++) {

            logger.debug("Voy a comprobar el punto [" + fila + "][" + (columna + i) + "] para el " + nombreBarco);
            if (columna + i > 9 || tablero.getTableroInferior()[fila][columna + i] != -2) {
                este = false;
                break;
            }
        }
        //return de boolean
        return este;
    }

    private void colocarBarcoSur(int num, int largo, int fila, int columna, int[][] tablero) {
        //Tapa de arriba
        if (fila - 1 > -1) {
            if (columna - 1 > 0) tablero[fila - 1][columna - 1] = -1;
            tablero[fila - 1][columna] = -1;
            if (columna + 1 < 10) tablero[fila - 1][columna + 1] = -1;
        }

        //Barco y bordes
        for (int i = 0; i < largo; i++) {

            logger.info("Voy a colocar el barco en el punto [" + (fila + i) + "][" + columna + "]");
            tablero[fila + i][columna] = num;
            if (columna - 1 > 0) tablero[fila + i][columna - 1] = -1;
            if (columna + 1 < 10) tablero[fila + i][columna + 1] = -1;

        }

        //Tapa de abajo
        if (fila + largo < 10) {
            if (columna - 1 > 0) tablero[fila + largo][columna - 1] = -1;
            tablero[fila + largo][columna] = -1;
            if (columna + 1 < 10) tablero[fila + largo][columna + 1] = -1;
        }
    }

    private boolean comprobarSur(int largo, int fila, int columna, String nombreBarco) {
        //crear boolean igual a true
        boolean sur = true;
        //bucle de comprobacion
        for (int i = 0; i < largo; i++) {

            logger.debug("Voy a comprobar el punto [" + (fila + i) + "][" + columna + "] para el " + nombreBarco);
            if (fila + i > 9 || tablero.getTableroInferior()[fila + i][columna] != -2) {
                sur = false;
                break;
            }
        }
        //return de boolean
        return sur;
    }

    private void colocarBarcoNorte(int num, int largo, int fila, int columna, int[][] tablero) {
        //Tapa de arriba
        if (fila - largo > -1) {
            if (columna - 1 > 0) tablero[fila - largo][columna - 1] = -1;
            tablero[fila - largo][columna] = -1;
            if (columna + 1 < 10) tablero[fila - largo][columna + 1] = -1;
        }

        //Barco y bordes
        for (int i = 0; i < largo; i++) {

            logger.info("Voy a colocar el barco en el punto [" + (fila - i) + "][" + columna + "]");
            tablero[fila - i][columna] = num;
            if (columna - 1 > 0) tablero[fila - i][columna - 1] = -1;
            if (columna + 1 < 10) tablero[fila - i][columna + 1] = -1;

        }

        //Tapa de abajo
        if (fila + 1 < 10) {
            if (columna - 1 > 0) tablero[fila + 1][columna - 1] = -1;
            tablero[fila + 1][columna] = -1;
            if (columna + 1 < 10) tablero[fila + 1][columna + 1] = -1;
        }
    }

    private boolean comprobarNorte(int largo, int fila, int columna, String nombreBarco) {
        //crear boolean igual a true
        boolean norte = true;
        //bucle de comprobacion
        for (int i = 0; i < largo; i++) {

            logger.debug("Voy a comprobar el punto [" + (fila - i) + "][" + columna + "] para el " + nombreBarco);
            if (fila - i < 0 || tablero.getTableroInferior()[fila - i][columna] != -2) {
                norte = false;
                break;
            }
        }
        //return de boolean
        return norte;
    }

    public void iniciarPartida() {
        String coordenada;
        int fila, columna;
        int contadorBarcos;
        Barco[] vecBarcosAux = flota.getFlotaBarcos().toArray(new Barco[0]);
        contadorBarcos = vecBarcosAux.length;

        while (jugador.getNumMovimentos() > 0 && contadorBarcos>0) {

            //mostrar tablero superior
            System.out.println("Tienes " + jugador.getNumMovimentos() + " tiradas restantes");
            tablero.mostrarTableroSup();
            //pedimos al usuario una letra y un num
            System.out.print("Introduce Fila/Columna: ");
            coordenada = scan.next(); //"A4"
            columna = Integer.parseInt(String.valueOf(coordenada.charAt(1))); //4
            fila = conversor.indexOf(String.valueOf(coordenada.charAt(0)).toUpperCase()); //"A"
            //comprobamos la posicion en el tablero inferior

            if (!tablero.comprobarPosicion(fila, columna)) {
                //mostramos el resultado en el tablero superior
                if (tablero.getTableroInferior()[fila][columna] == -1 || tablero.getTableroInferior()[fila][columna] == -2) {
                    System.out.println("Agua");
                    //pintar una O en talbero superior
                    tablero.marcarTableroSuperior(fila, columna, "O");
                    tablero.marcarPosicion(fila, columna);
                    jugador.restarMovimiento();
                } else {
                    int barco = tablero.getTableroInferior()[fila][columna];
                    System.out.println("TOCADO!");
                    vecBarcosAux[barco].tocado();
                    if (vecBarcosAux[barco].isHundido()) {
                        System.out.println("Y HUNDIDO! Has hundido un " + vecBarcosAux[barco].getNombreBarco());
                        contadorBarcos--;
                    }
                    tablero.marcarTableroSuperior(fila, columna, "X");
                    tablero.marcarPosicion(fila, columna);

                    jugador.restarMovimiento();
                }
            } else {
                System.out.println("## Ya has comprobado esta casilla! ##");
            }

        }
        //SI NO TENGO MOVIMIENTOS, HE PERDIDO, SI NO QUEDAN BARCOS HE GANADO
        if (jugador.getNumMovimentos() < 1) System.out.println("FIN PARTIDA!!! Te has quedado sin movimientos");
        else if(contadorBarcos==0) System.out.println("HAS GANADO!!! Has hundido todos los barcos");
    }
}

